#ifndef PERMITTEDCOMMANDS_H
#define PERMITTEDCOMMANDS_H

#include <string>
#include <vector>
#include <mutex>

class PermittedCommands
{
public:
    PermittedCommands(const std::string &filename);
    void reload();
    bool is_permitted(const std::string &command_line) const;

private:
    std::string m_filename;
    std::vector<std::string> m_commands;
    mutable std::mutex m_mutex;
};

#endif // PERMITTEDCOMMANDS_H
