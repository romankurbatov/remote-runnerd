TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -pthread

SOURCES += \
        logger.cpp \
        main.cpp \
        permitted_commands.cpp \
        serve_client.cpp

HEADERS += \
    logger.h \
    permitted_commands.h \
    serve_client.h \
    socket.h
