#include "serve_client.h"

#include <string>
#include <future>
#include <cstring>
#include <cerrno>
#include <csignal>

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "socket.h"
#include "logger.h"
#include "permitted_commands.h"

static const size_t buffer_size = 1024;

static const char not_permitted_message[] = "ERROR: Command not permitted\n";
static const size_t not_permitted_message_length =
        sizeof(not_permitted_message)/sizeof(char);

static const char timeout_exceeded_message[] = "ERROR: Timeout exceeded\n";
static const size_t timeout_exceeded_message_length =
        sizeof(timeout_exceeded_message)/sizeof(char);

static const char prompt[] = "> ";
static const size_t prompt_length = sizeof(prompt)/sizeof(char);

// Timeout between sending a child SIGTERM and SIGKILL
static const unsigned int timeout_before_kill = 100; // in milliseconds

static void kill_with_timeout(pid_t pid, unsigned int timeout);

void serve_client(
        Socket socket,
        int timeout,
        std::shared_future<void> terminate_future,
        const PermittedCommands &permitted_commands,
        Logger &logger)
{
    // Block all signals in this thread. We want to handle signals in main thread
    {
        sigset_t set;
        sigfillset(&set);
        pthread_sigmask(SIG_BLOCK, &set, NULL);
    }

    std::string request;

    {
        int flags = fcntl(socket.raw(), F_GETFL, 0);
        flags |= O_NONBLOCK;
        fcntl(socket.raw(), F_SETFL, flags);
    }

    std::unique_ptr<char[]> buffer(new char[buffer_size]);

    // Processing requests
    while (true) {
        request.clear();

        // Getting a request
        while (true) {
            ssize_t count = -1;

            write(socket.raw(), prompt, prompt_length);

            // Getting a portion of the request
            while (true) {
                if (terminate_future.wait_for(std::chrono::seconds(0)) ==
                        std::future_status::ready) {
                    // Main thread signals to terminate
                    return;
                }

                count = read(socket.raw(), buffer.get(), buffer_size);
                if (count < 0 && errno != EAGAIN && errno != EWOULDBLOCK) {
                    logger.report_error(std::string("Unable to read a request: ") +
                                        strerror(errno));
                    return;
                } else if (count >= 0) {
                    // Got a portion
                    break;
                }

                std::this_thread::sleep_for(std::chrono::milliseconds(1));
            }

            request.append(buffer.get(), count);

            if (count == 0 || buffer[count - 1] == '\n') {
                // Got full request
                break;
            }
        }

        if (request.empty()) {
            // No more requests - terminating the thread
            break;
        }

        if (request.back() == '\n') {
            request.pop_back();
        }

        if (request.back() == '\r') {
            request.pop_back();
        }

        if (request.empty()) {
            // Skip empty command
            continue;
        }

        if (!permitted_commands.is_permitted(request)) {
            write(socket.raw(), not_permitted_message, not_permitted_message_length);
            continue;
        }

        pid_t child_pid = fork();
        if (child_pid == -1) {
            logger.report_error(std::string("Unable to fork: ") +
                                strerror(errno));
            return;
        }
        if (child_pid == 0) {
            // Child process
            dup2(socket.raw(), STDOUT_FILENO);
            dup2(socket.raw(), STDERR_FILENO);
            close(socket.raw());
            close(STDIN_FILENO);
            execlp("sh", "sh", "-c", request.c_str(), NULL);

            // Reach this point only on error
            logger.report_error(std::string("Unable to exec: ") +
                                strerror(errno));
            return;
        }

        auto start = std::chrono::steady_clock::now();

        while (true) {
            // Check if the process finished
            if (0 != waitpid(child_pid, NULL, WNOHANG)) {
                break;
            }

            // Check if it is time to terminate
            if (terminate_future.wait_for(std::chrono::seconds(0)) ==
                    std::future_status::ready) {
                kill_with_timeout(child_pid, timeout_before_kill);
                return;
            }

            // Check if timeout exceeded
            if (timeout > 0) {
                auto now = std::chrono::steady_clock::now();
                auto seconds_spent =
                        std::chrono::duration_cast<std::chrono::seconds>(now - start).count();
                if (seconds_spent > timeout) {
                    kill_with_timeout(child_pid, timeout_before_kill);
                    write(socket.raw(),
                          timeout_exceeded_message,
                          timeout_exceeded_message_length);
                    break;
                }
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }
}

static void kill_with_timeout(pid_t pid, unsigned int timeout) {
    kill(pid, SIGTERM);
    auto start = std::chrono::steady_clock::now();
    while (true) {
        if (0 != waitpid(pid, NULL, WNOHANG)) {
            return;
        }

        auto now = std::chrono::steady_clock::now();
        auto milliseconds_spent =
                std::chrono::duration_cast<std::chrono::milliseconds>(now - start).count();
        if (milliseconds_spent > timeout) {
            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    kill(pid, SIGKILL);
    waitpid(pid, NULL, 0);
}
