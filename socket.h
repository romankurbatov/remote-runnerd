#ifndef SOCKET_H
#define SOCKET_H

#include <string>
#include <cstring>
#include <cerrno>
#include <unistd.h>

#include "logger.h"

// Auto-closeable socket wrapper
class Socket
{
public:
    Socket(int socket, Logger &logger, const std::string &filename = std::string());
    Socket(Socket &&donor);
    ~Socket();

    int raw();

private:
    int m_socket;
    Logger &m_logger;
    std::string m_filename;
};

inline Socket::Socket(int socket, Logger &logger, const std::string &filename) :
    m_socket(socket),
    m_logger(logger),
    m_filename(filename)
{
}

inline Socket::Socket(Socket &&donor) :
    m_socket(donor.m_socket),
    m_logger(donor.m_logger),
    m_filename(std::move(donor.m_filename))

{
    donor.m_socket = -1;
}

inline Socket::~Socket() {
    if (-1 != m_socket) {
        int retval = close(m_socket);
        if (0 != retval) {
            m_logger.report_error(std::string("Unable to close socket: ") + strerror(errno));
        }

        if (!m_filename.empty()) {
            int retval = unlink(m_filename.c_str());
            if (0 != retval) {
                m_logger.report_error(std::string("Unable to remove socket file: ")
                                      + strerror(errno));
            }
        }
    }
}

inline int Socket::raw() {
    return m_socket;
}

#endif // SOCKET_H
