#include <iostream>
#include <cstdlib>
#include <cerrno>
#include <cstring>
#include <csignal>
#include <string>
#include <thread>
#include <future>
#include <utility>
#include <unordered_map>
#include <atomic>
#include <functional>
#include <stdexcept>

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "serve_client.h"
#include "permitted_commands.h"
#include "logger.h"
#include "socket.h"

const char *help_message =
"remote-runnerd                                                     \n"
"    remotely execute commands from list of permitted commands      \n"
"                                                                   \n"
"                                                                   \n"
"remote-runnerd --help                                              \n"
"   show this help message                                          \n"
"                                                                   \n"
"                                                                   \n"
"remote-runnerd [--port=PORT] [--timeout=TIMEOUT] [--config=CONFIG] \n"
"               [--log=LOG_BASE] [--local=LOCAL_SOCKET]             \n"
"                                                                   \n"
"   --port      Port.                                               \n"
"               Default value: 12345.                               \n"
"   --timeout   Command execution timeout.                          \n"
"               Default value: no timeout.                          \n"
"   --config    File with permitted commands.                       \n"
"               Default value: /etc/remote-runnerd.conf.            \n"
"   --log       Base of logfile name. PID will be appended.         \n"
"               Default value: /tmp/remote-runnerd.log              \n"
"   --local     Name of local socket file.                          \n"
"               Default value: none.                                \n"
"                                                                   \n"
"   You cannot specify both --port and --local.                     \n"
"                                                                   \n";

static const int max_queue = 16;
static const size_t max_local_socket_filename = 100;

static std::atomic<bool> got_sigterm_sigint = false;
static std::atomic<bool> got_sighup = false;

struct Arguments {
    int port;
    int timeout;
    std::string config_filename;
    std::string log_filename_base;
    std::string local_socket_filename;
    bool show_help;
};

void process_sigterm_sigint(int);
void process_sighup(int);

void extract_args(int argc, char *argv[], Arguments &arguments);
int run(const Arguments &arguments);

int main(int argc, char *argv[])
{
    {
        signal(SIGPIPE, SIG_IGN);

        struct sigaction action, old_action;
        action.sa_handler = &process_sigterm_sigint;
        sigemptyset(&action.sa_mask);
        action.sa_flags = 0;

        sigaction(SIGTERM, &action, &old_action);
        sigaction(SIGINT, &action, &old_action);

        action.sa_handler = &process_sighup;
        sigemptyset(&action.sa_mask);
        action.sa_flags = 0;

        sigaction(SIGHUP, &action, &old_action);
    }

    Arguments arguments = {
        12345,                          // port
        -1,                             // timeout
        "/etc/remote-runnerd.conf",     // configuration file
        "/tmp/remote-runnerd.log",      // log file (PID will be appended later)
        "",                             // don't use local socket by default
        false                           // show help and exit?
    };

    try {
        extract_args(argc, argv, arguments);
    } catch (const std::runtime_error &e) {
        std::cerr << argv[0] << ": " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    if (arguments.show_help) {
        std::cerr << help_message << std::endl;
        return EXIT_SUCCESS;
    }

    pid_t child_pid = fork();
    if (child_pid == -1) {
        std::cerr << argv[0] << ": "
                  << "Unable to fork: "
                  << strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }
    if (child_pid > 0) {
        // Parent process
        std::cerr << argv[0] << ": "
                  << "Daemon started with PID "
                  << child_pid << std::endl;
        return EXIT_SUCCESS;
    }

    // Child process
    return run(arguments);
}

void process_sigterm_sigint(int) {
    got_sigterm_sigint = true;
}

void process_sighup(int) {
    got_sighup = true;
}

void extract_args(int argc, char *argv[], Arguments &arguments) {
    bool port_specified = false;
    bool local_socket_filename_specified = false;

    for (int i = 1; i < argc; ++i) {
        const std::string argument = argv[i];

        if (argument == "--help") {
            arguments.show_help = true;
            break;
        }

        const size_t equal_sign_pos = argument.find('=');
        if (std::string::npos == equal_sign_pos) {
            throw std::runtime_error(std::string("Invalid argument '") +
                                     argument + "'");
        }

        const std::string key = argument.substr(0, equal_sign_pos);
        const std::string value = argument.substr(equal_sign_pos + 1);
        if (key == "--port") {
            arguments.port = atoi(value.c_str());
            if (arguments.port <= 0) {
                throw std::runtime_error(
                            std::string("Port must be positive number"));
            }
            port_specified = true;
        } else if (key == "--timeout") {
            arguments.timeout = atoi(value.c_str());
            if (arguments.timeout <= 0) {
                throw std::runtime_error(
                            std::string("Timeout must be positive number"));
            }
        } else if (key == "--config") {
            arguments.config_filename = value;
        } else if (key == "--log") {
            arguments.log_filename_base = value;
        } else if (key == "--local") {
            if (value.size() > max_local_socket_filename) {
                throw std::runtime_error(
                            std::string("Local socket filename cannot be longer ") +
                            std::to_string(max_local_socket_filename));
            }
            arguments.local_socket_filename = value;
            local_socket_filename_specified = true;
        } else {
            throw std::runtime_error(std::string("Unknown argument '") +
                                     key + "'");
        }
    }

    if (port_specified && local_socket_filename_specified) {
        throw std::runtime_error("You cannot specify both port and local socket");
    }
}

int run(const Arguments &arguments) {
    PermittedCommands permitted_commands(arguments.config_filename);

    std::string log_filename(arguments.log_filename_base + "." + std::to_string(getpid()));
    Logger logger(log_filename);

    bool is_local = !arguments.local_socket_filename.empty();

    int raw_listening_socket = -1;
    if (is_local) {
        raw_listening_socket = socket(AF_UNIX, SOCK_STREAM, 0);
    } else {
        raw_listening_socket = socket(AF_INET, SOCK_STREAM, 0);
    }

    if (-1 == raw_listening_socket) {
        logger.report_fatal(std::string("Unable to create a socket: ") +
                            strerror(errno));
        return EXIT_FAILURE;
    }

    Socket listening_socket(raw_listening_socket, logger, arguments.local_socket_filename);

    int retval = -1;
    if (is_local) {
        sockaddr_un address;
        address.sun_family = AF_UNIX;
        strcpy(address.sun_path, arguments.local_socket_filename.c_str());
        retval = bind(listening_socket.raw(),
                      reinterpret_cast<sockaddr *>(&address),
                      sizeof(address));
    } else {
        sockaddr_in address;
        address.sin_family = AF_INET;
        address.sin_port = htons(arguments.port);
        address.sin_addr.s_addr = htonl(INADDR_ANY);
        retval = bind(listening_socket.raw(),
                      reinterpret_cast<sockaddr *>(&address),
                      sizeof(address));
    }

    if (0 != retval) {
        logger.report_fatal(std::string("Unable to bind an address to a socket: ") +
                            strerror(errno));
        return EXIT_FAILURE;
    }

    retval = listen(listening_socket.raw(), max_queue);
    if (0 != retval) {
        logger.report_fatal(std::string("Unable to start listening: ") +
                            strerror(errno));
        return EXIT_FAILURE;
    }

    // Promise to signal threads to terminate
    std::promise<void> terminate_promise;
    std::shared_future<void> terminate_future(terminate_promise.get_future());

    std::unordered_map<std::thread::id, std::thread> threads;
    // Futures to signal main thread that other threads finish
    std::unordered_map<std::thread::id, std::future<void>> finish_futures;

    while (true) {
        if (got_sighup) {
            permitted_commands.reload();
            got_sighup = false;
        }

        if (got_sigterm_sigint) {
            break;
        }

        int raw_accepted_socket = accept(listening_socket.raw(), NULL, NULL);
        if (-1 == raw_accepted_socket) {
            if (EINTR == errno && (got_sigterm_sigint || got_sighup)) {
                continue;
            } else {
                logger.report_fatal(std::string("Unable to accept a connection: ") +
                                    strerror(errno));
                return EXIT_FAILURE;
            }
        }

        Socket accepted_socket(raw_accepted_socket, logger);

        // Create and run new thread to serve a new client
        std::packaged_task
                <void (Socket, int, std::shared_future<void>,
                       const PermittedCommands &, Logger &)>
                task(serve_client);
        std::future<void> finish_future = task.get_future();
        std::thread thread(std::move(task),
                           std::move(accepted_socket),
                           arguments.timeout,
                           terminate_future,
                           std::cref(permitted_commands),
                           std::ref(logger));
        const std::thread::id thread_id = thread.get_id();
        threads.try_emplace(thread_id, std::move(thread));
        finish_futures.try_emplace(thread_id, std::move(finish_future));

        // If some of threads finished, do cleanup
        for (auto iter = finish_futures.begin(); iter != finish_futures.end(); ) {
            const std::thread::id thread_id = iter->first;
            if (iter->second.wait_for(std::chrono::seconds(0)) == std::future_status::ready) {
                threads[thread_id].join();
                threads.erase(thread_id);
                iter = finish_futures.erase(iter);
            } else {
                ++iter;
            }
        }
    }

    // Signal threads to terminate
    terminate_promise.set_value();

    // Wait until all threads terminate and do cleanup
    for (auto iter = threads.begin(); iter != threads.end(); ) {
        const std::thread::id thread_id = iter->first;
        iter->second.join();
        iter = threads.erase(iter);
        finish_futures.erase(thread_id);
    }

    return EXIT_SUCCESS;
}
