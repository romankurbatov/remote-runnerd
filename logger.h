#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <fstream>
#include <mutex>

class Logger
{
public:
    Logger(const std::string &filename);
    void report_error(const std::string &message);
    void report_fatal(const std::string &message);

private:
    std::ofstream m_logfile;
    std::mutex m_mutex;
};

#endif // LOGGER_H
