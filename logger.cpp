#include "logger.h"

#include <iostream>

Logger::Logger(const std::string &filename) :
    m_logfile(filename)
{
    if (!m_logfile.is_open()) {
        std::cerr << "Unable to open log file '" << filename << "'" << std::endl;
    }

    m_logfile.flush();
}

void Logger::report_error(const std::string &message) {
    std::scoped_lock<std::mutex> lock(m_mutex);
    m_logfile << "ERROR: " << message << std::endl;
    m_logfile.flush();
}

void Logger::report_fatal(const std::string &message) {
    std::scoped_lock<std::mutex> lock(m_mutex);
    m_logfile << "FATAL: " << message << std::endl;
    m_logfile.flush();
}
