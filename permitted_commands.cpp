#include "permitted_commands.h"

#include <fstream>
#include <cctype>
#include <iostream>

PermittedCommands::PermittedCommands(const std::string &filename) :
    m_filename(filename)
{
    reload();
}

void PermittedCommands::reload() {
    std::scoped_lock<std::mutex> lock(m_mutex);
    std::ifstream file(m_filename);
    if (!file.is_open()) {
        std::cerr << "Unable to open the file" << std::endl;
    }
    m_commands.clear();
    std::string command;
    while (file >> command) {
        m_commands.push_back(command);
    }
}

static bool starts_with_command(const std::string &command_line, const std::string &command) {
    for (size_t i = 0; i < command.size(); ++i) {
        if (i >= command_line.size() || command_line[i] != command[i]) {
            return false;
        }
    }

    // If command line is longer than command and next character is not space,
    // then user is calling another command
    if (command_line.size() != command.size() && !isspace(command_line[command.size()])) {
        return false;
    }

    return true;
}

bool PermittedCommands::is_permitted(const std::string &command_line) const {
    std::scoped_lock<std::mutex> lock(m_mutex);

    for (const std::string &command : m_commands) {
        if (starts_with_command(command_line, command)) {
            return true;
        }
    }

    return false;
}
