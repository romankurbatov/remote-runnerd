#ifndef SERVE_CLIENT_H
#define SERVE_CLIENT_H

#include <future>

#include "socket.h"
#include "logger.h"
#include "permitted_commands.h"

void serve_client(
        Socket socket,
        int timeout,
        std::shared_future<void> terminate_future,
        const PermittedCommands &permitted_commands,
        Logger &logger);

#endif // SERVE_CLIENT_H
